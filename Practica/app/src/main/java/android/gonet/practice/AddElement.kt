package android.gonet.practice

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.gonet.practice.R
import android.gonet.practice.view.MainActivity
import kotlinx.android.synthetic.main.activity_add_element.*

class AddElement : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_element)

        btn_add_new_element.setOnClickListener {

            val tipo = et_add_element.text.toString()
            println(tipo)

            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("Elemento", et_add_element.text.toString())
            startActivity(intent)

        }
    }
}
