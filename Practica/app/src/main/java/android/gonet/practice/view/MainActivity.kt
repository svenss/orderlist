package android.gonet.practice.view

import android.content.Intent
import android.gonet.practice.AddElement
import android.gonet.practice.R
import android.gonet.practice.adapter.AdapterRecycler
import android.gonet.practice.model.ElementList
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val faa = intent.getStringExtra("Elemento")

        val listaDos:ArrayList<ElementList> = arrayListOf()

        var listaArreglo = resources.getStringArray(R.array.string_array)


        if(faa !=  null) {

            val listaUno = ElementList(0,faa)
            listaDos!!.add(listaUno)
            listaDos.sort()

        }



        fun getLists(): ArrayList<ElementList> {

            /*if(faa != null){

                val lists = ArrayList<ElementList>()
                lists.add(ElementList(0,"Excelente"))
                lists.add(ElementList(1,"Aplicable"))
                lists.add(ElementList(2,"Buena App"))
                lists.add(ElementList(3,"Gonet"))
                lists.add(ElementList(4, faa.toString()))
                Collections.sort(lists)
                return lists


            }else if (faa == null){
                val lists = ArrayList<ElementList>()
                lists.add(ElementList(0,"Elemento uno de la lista"))
                lists.add(ElementList(1,"Elemento uno de la lista.."))
                lists.add(ElementList(2,"Elemento uno de la lista...."))
                lists.add(ElementList(3,"Elemento uno de la lista......."))

            }*/



            for (i in listaArreglo){

                val listaUno = ElementList(0,i)
                listaDos!!.add(listaUno)
                listaDos.sort()


            }

         return arrayListOf()
        }


        val recyclerView: RecyclerView?
        val adapter: RecyclerView.Adapter<*>?
        val layotManager: RecyclerView.LayoutManager?

       // val response:MutableList<ElementList> = getLists()

        getLists()

        recyclerView = findViewById(R.id.recycler_menu)
        layotManager = LinearLayoutManager(this)

        adapter = AdapterRecycler(
            listaDos,
            R.layout.adapter_element_list
        )

        recyclerView?.adapter = adapter
        recyclerView?.layoutManager = layotManager

        btn_add_element.setOnClickListener {

            val intent = Intent(this, AddElement::class.java)
            startActivity(intent)
        }
    }

}
