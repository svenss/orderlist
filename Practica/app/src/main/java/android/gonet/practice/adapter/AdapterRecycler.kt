package android.gonet.practice.adapter

import android.content.Context
import android.gonet.practice.R
import android.gonet.practice.model.ElementList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.adapter_element_list.view.*
import java.util.*
import kotlin.math.acos

class AdapterRecycler(
    var response:MutableList<ElementList>,
    var layout:Int
    //var listener: OnItemClickListener
):RecyclerView.Adapter<AdapterRecycler.ViewHolder>() {



    init {
        this.response = response
        this.layout = layout
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterRecycler.ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.adapter_element_list, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return response.size
    }

    override fun onBindViewHolder(p0: AdapterRecycler.ViewHolder, p1: Int) {
        p0.bind(itemCount, response, layout)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) ,View.OnClickListener{
        override fun onClick(v: View?) {

            if(v == itemView.iv_remove_element){
                response.removeAt(adapterPosition)
                this@AdapterRecycler.notifyDataSetChanged()
            }

        }


        fun bind(
            count: Int,
            response: MutableList<ElementList>,
            layout: Int
        ) {

            itemView.tv_element_list.text = response[adapterPosition].descripcion

            itemView.iv_remove_element.setOnClickListener(this)

            val trae = itemView.chbox_menu

            response.sortBy { response.size }


            itemView.chbox_menu.setOnClickListener {

                var tipo: Boolean = itemView.chbox_menu.isChecked

                print(tipo)

                if (tipo){


                val traer: String = response[adapterPosition].descripcion
                response.removeAt(adapterPosition)
                    val pos:Int = response[adapterPosition].position
                    val model = ElementList(pos,traer)
                response.add(model)
                   this@AdapterRecycler.notifyItemMoved(adapterPosition, response.size-1)
                    Collections.sort(response)



               }else{
                    val mover: String = response[adapterPosition].descripcion
                    response.removeAt(adapterPosition)
                    val pos:Int = response[0].position
                    val model = ElementList(pos,mover)
                    response.add(model)
                    this@AdapterRecycler.notifyItemMoved(adapterPosition, pos)
                    Collections.sort(response)


                }

            }

        }

    }

}



