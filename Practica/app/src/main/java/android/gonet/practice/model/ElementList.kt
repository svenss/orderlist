package android.gonet.practice.model


data class ElementList(
    val position:Int,
    val descripcion:String
):Comparable<ElementList> {
    override fun compareTo(other: ElementList): Int {
        return descripcion.compareTo(other.descripcion)
    }
}