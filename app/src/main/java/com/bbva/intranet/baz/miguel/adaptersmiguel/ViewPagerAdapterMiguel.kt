package com.bbva.intranet.baz.miguel.adaptersmiguel

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

open class ViewPagerAdapterMiguel: FragmentPagerAdapter {


    private var fragmentList: ArrayList<Fragment> = ArrayList()

    constructor(fm:FragmentManager, fragmentList:List<Fragment>):super(fm){
        this.fragmentList = fragmentList as ArrayList<Fragment>
    }


    override fun getItem(p0: Int): Fragment {
        return fragmentList[p0]
    }

    override fun getCount(): Int {
       return fragmentList.size
    }


}