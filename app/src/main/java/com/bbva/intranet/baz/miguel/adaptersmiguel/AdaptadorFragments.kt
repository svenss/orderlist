package com.bbva.intranet.baz.miguel.adaptersmiguel

import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.bbva.intranet.baz.R

import com.bbva.intranet.baz.miguel.fragmentsmiguel.FragmentGet
import com.bbva.intranet.baz.miguel.fragmentsmiguel.FragmentPost
import com.bbva.intranet.baz.miguel.viewmiguel.InicioActivity


class AdaptadorFragments : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services_ws)

        val frPost:Fragment
        val frGet:Fragment

        frPost = FragmentPost()
        frGet = FragmentGet()





        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container_post, frPost)
        fragmentTransaction.commit()

        val fmget = supportFragmentManager
        val fragmentTransactionget = fmget.beginTransaction()
        fragmentTransactionget.replace(R.id.fragment_container_get, frGet)
        fragmentTransactionget.commit()


    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflat= menuInflater
        inflat.inflate(R.menu.menu_back_miguel, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {


        when(item!!.itemId){
            R.id.back_miguel -> {

                val intent = Intent(this, InicioActivity::class.java)
                startActivity(intent)
                return true
            }else -> return super.onOptionsItemSelected(item)
        }
    }

}
