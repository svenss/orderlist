package com.bbva.intranet.baz.miguel.fragmentsmiguel

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.bbva.intranet.baz.R
import com.bbva.intranet.baz.miguel.viewmiguel.DetallesMiguelActivity
import com.bbva.intranet.baz.miguel.viewmiguel.InicioActivity.Companion.NAME
import kotlinx.android.synthetic.main.activity_inicio.*


class FragmentPost : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.activity_fragment_post, container, false)



        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val btnDetalles:Button = view.findViewById(R.id.btn_detalles_post)

        val tvNombre:TextView = view.findViewById(R.id.tv_nombre_fragment)

        val tvEdad:TextView = view.findViewById(R.id.tv_edad_fragment)

        val pref:SharedPreferences = activity!!.getSharedPreferences(NAME, Context.MODE_PRIVATE)


        val edad = pref.getString("edad","")
        val nombre = pref.getString("nombre", "")

        tvNombre.text = nombre

        tvEdad.text = edad


        btnDetalles.setOnClickListener {

            val intent = Intent(activity, DetallesMiguelActivity::class.java)
            activity!!.startActivity(intent)
            activity!!.finish()
        }



    }



}