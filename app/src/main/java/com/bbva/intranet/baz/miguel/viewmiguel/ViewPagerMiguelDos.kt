package com.bbva.intranet.baz.miguel.viewmiguel

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bbva.intranet.baz.R
import com.bumptech.glide.Glide

class ViewPagerMiguelDos:Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewDos:ViewGroup = inflater.inflate(R.layout.miguel_view_pager_dos, container, false) as ViewGroup

        return  viewDos


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val fotoDos:ImageView = view.findViewById(R.id.foto_viewpager_localidad)


        val pref: SharedPreferences = activity!!.getSharedPreferences(InicioActivity.NAME, Context.MODE_PRIVATE)


        val photo = pref.getString("photo","")


        Glide.with(context!!)
                .load(photo)
                .into(fotoDos)


    }

}