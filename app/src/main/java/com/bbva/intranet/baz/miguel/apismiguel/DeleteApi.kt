package com.bbva.intranet.baz.miguel.apismiguel

import com.bbva.intranet.baz.miguel.modelmiguel.ModelGet
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.DELETE

interface DeleteApi {

    @DELETE("/detail/")
    fun sendDelete():Call<List<ModelGet>>

    companion object{


        fun crearCliente():DeleteApi{

            val retrofit = Retrofit.Builder()
                    .baseUrl("http://5ca637e23a082600142794d8.mockapi.io/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return  retrofit.create(DeleteApi::class.java)

        }

    }




}