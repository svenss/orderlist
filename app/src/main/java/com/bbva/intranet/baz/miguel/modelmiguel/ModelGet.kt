package com.bbva.intranet.baz.miguel.modelmiguel

data class ModelGet(
        val id: String,
        val localidad: String,
        val photo: String,
        val sexo: String
)