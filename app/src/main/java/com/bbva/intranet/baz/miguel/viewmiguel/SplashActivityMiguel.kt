package com.bbva.intranet.baz.miguel.viewmiguel

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.bbva.intranet.baz.R

class SplashActivityMiguel : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_miguel)


        Handler().postDelayed({
            val i = Intent(this, InicioActivity::class.java)
            startActivity(i)
            finish()
        }, 4000)
    }
}
