package com.bbva.intranet.baz.miguel.viewmiguel

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import com.bbva.intranet.baz.R
import com.bbva.intranet.baz.miguel.adaptersmiguel.AdaptadorFragments
import com.bbva.intranet.baz.miguel.adaptersmiguel.ViewPagerAdapterMiguel

class ViewPagerMiguel : AppCompatActivity() {

    private var pager:ViewPager ? = null
    private var pagerAdapter:PagerAdapter ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pager_miguel)

        val fragmentList = ArrayList<Fragment>()
        fragmentList.add(ViewPagerMiguelUno())/*vistas a pager*/
        fragmentList.add(ViewPagerMiguelDos())

        pager = findViewById(R.id.view_pager_container_miguel)
        pagerAdapter = ViewPagerAdapterMiguel(supportFragmentManager, fragmentList)
        pager!!.adapter = pagerAdapter

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflat= menuInflater
        inflat.inflate(R.menu.menu_back_miguel, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {


        when(item!!.itemId){
            R.id.back_miguel -> {

                val intent = Intent(this, AdaptadorFragments::class.java)
                startActivity(intent)
                return true
            }else -> return super.onOptionsItemSelected(item)
        }
    }
}
