package com.bbva.intranet.baz.miguel.adaptersmiguel

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bbva.intranet.baz.R
import com.bbva.intranet.baz.miguel.apismiguel.DeleteApi
import com.bbva.intranet.baz.miguel.apismiguel.GetApi
import com.bbva.intranet.baz.miguel.modelmiguel.ModelGet
import com.bbva.intranet.baz.miguel.viewmiguel.InicioActivity
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Response

class RecyclerAdapter(var response:List<ModelGet>,
                      var layout: Int):RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    var itemClickListener: AdapterView.OnItemClickListener? = null

    init {
        this.response = response
        this.layout = layout
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(layout, p0, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bind(itemCount, response, layout)
    }


    override fun getItemCount(): Int {
        return  response.size
    }



    class ViewHolder(view: View):RecyclerView.ViewHolder(view){



        var tv_genero:TextView
        var tv_ciudad:TextView
        var iv_imagen: ImageView
        var tv_borrar:TextView

        init {
            tv_genero = view.findViewById(R.id.genero_recycler_miguel)
            tv_ciudad = view.findViewById(R.id.ciudad_recycler_miguel)
            iv_imagen = view.findViewById(R.id.iv_recycler_miguel)
            tv_borrar = view.findViewById(R.id.tv_borrar_miguel)
        }

        @SuppressLint("SetTextI18n")
        fun bind(item:Int, response:List<ModelGet>, layout:Int){
            tv_genero.text = response.get(adapterPosition).sexo
            tv_ciudad.text = response.get(adapterPosition).localidad
            tv_borrar.text = "Eliminar"

            Glide.with(itemView.context)
                    .load(response.get(adapterPosition).photo)
                    .into(iv_imagen)

            tv_borrar.setOnClickListener {
                val ide = response.get(adapterPosition).id

                print(ide + " *************")



                eliminarItem()
            }


        }

        fun  eliminarItem(){

            val respuesta = DeleteApi.crearCliente()
            val call = respuesta.sendDelete()

            call.enqueue(object : retrofit2.Callback<List<ModelGet>>{



                override fun onResponse(call: Call<List<ModelGet>>, response: Response<List<ModelGet>>) {

                    response.body()
                }

                override fun onFailure(call: Call<List<ModelGet>>, t: Throwable) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }


            })


        }

    }


}