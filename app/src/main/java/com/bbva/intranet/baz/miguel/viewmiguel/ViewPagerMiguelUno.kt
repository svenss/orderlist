package com.bbva.intranet.baz.miguel.viewmiguel

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bbva.intranet.baz.R

open class ViewPagerMiguelUno:Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val viewUno:ViewGroup = inflater.inflate(R.layout.miguel_view_pager_uno, container, false) as ViewGroup

        return viewUno
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val tvLocalidad:TextView = view.findViewById(R.id.viewpager_localidad_miguel)
        val tvGenero:TextView = view.findViewById(R.id.viewpager_genero_miguel)


        val pref: SharedPreferences = activity!!.getSharedPreferences(InicioActivity.NAME, Context.MODE_PRIVATE)


        val localidad = pref.getString("genero","")
        val genero = pref.getString("localidad", "")

        print(localidad + " " + genero)

        tvLocalidad.text = localidad
        tvGenero.text = genero



    }
}