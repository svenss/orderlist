package com.bbva.intranet.baz.miguel.fragmentsmiguel

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.bbva.intranet.baz.R
import com.bbva.intranet.baz.miguel.viewmiguel.ViewPagerMiguel

class FragmentGet: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
     val view = inflater.inflate(R.layout.activity_fragment_get, container, false)

     return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val irPager:Button = view.findViewById(R.id.btn_viewpager_miguel)

        irPager.setOnClickListener {

            val intent = Intent(activity, ViewPagerMiguel::class.java)
             activity!!.startActivity(intent)
             activity!!.finish()


        }


    }





}