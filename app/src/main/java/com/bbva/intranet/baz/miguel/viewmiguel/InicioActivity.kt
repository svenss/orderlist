package com.bbva.intranet.baz.miguel.viewmiguel

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.bbva.intranet.baz.R
import com.bbva.intranet.baz.miguel.adaptersmiguel.AdaptadorFragments
import com.bbva.intranet.baz.miguel.apismiguel.PostApi
import com.bbva.intranet.baz.miguel.modelmiguel.ResponsePost
import com.bbva.intranet.baz.miguel.modelmiguel.SendPost
import com.bbva.intranet.baz.miguel.webservices.WebServicePost
import retrofit2.Call
import retrofit2.Response

class InicioActivity : AppCompatActivity() {

    companion object{

        const val NAME = "miguel"

    }

    var progressDialog:ProgressDialog? = null

    @SuppressLint("WrongConstant", "CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio)


        val etNombre:EditText = findViewById(R.id.et_nombre_completo_miguel)
        val etEdad:EditText = findViewById(R.id.et_edad_miguel)


        val pref:SharedPreferences = getSharedPreferences(NAME, MODE_PRIVATE)
        val editor:SharedPreferences.Editor = pref.edit()

        val btnWs = findViewById<Button>(R.id.btn_post_miguel)

        btnWs.setOnClickListener {

            if(etNombre.text.toString() == "" || etEdad.text.toString() == ""){

                Toast.makeText(applicationContext, "El nombre o la edad estan vacios ", Toast.LENGTH_SHORT).show()

            }else{

                val intent = Intent(this, AdaptadorFragments::class.java)

                editor.putString("nombre", etNombre.text.toString())
                editor.putString("edad", etEdad.text.toString())
                editor.apply()

                peticionPost()

                progressDialog = ProgressDialog(this)
                progressDialog!!.setMessage("Cargando....")
                progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
                progressDialog!!.show()

                startActivity(intent)


            }

        }

    }





    fun peticionPost(){

        val pref: SharedPreferences = getSharedPreferences(NAME, Context.MODE_PRIVATE)

        //pref.getString("nombre", "")
        //pref.getString("edad", "")

        val edad = pref.getString("edad","")
        val nombre = pref.getString("nombre", "")

        val pasoDeDatos = SendPost(nombre.toString(), edad.toString())

        val envio = PostApi.crearCliente()
        val call = envio.sendPost(pasoDeDatos)

        call.enqueue(object: retrofit2.Callback<ResponsePost> {

            override fun onResponse(call: Call<ResponsePost>, response: Response<ResponsePost>) {
                response.body()
            }

            override fun onFailure(call: Call<ResponsePost>, t: Throwable) {
                Toast.makeText(applicationContext, "Fallo la comunicacion al servidor", Toast.LENGTH_SHORT).show()
            }

        })

    }


}
