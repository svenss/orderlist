package com.bbva.intranet.baz.miguel.viewmiguel

import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast

import android.widget.*
import com.bbva.intranet.baz.R
import com.bbva.intranet.baz.miguel.adaptersmiguel.AdaptadorFragments

import com.bbva.intranet.baz.miguel.adaptersmiguel.RecyclerAdapter
import com.bbva.intranet.baz.miguel.apismiguel.GetApi
import com.bbva.intranet.baz.miguel.modelmiguel.ModelGet
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList



@Suppress("UNREACHABLE_CODE")
class DetallesMiguelActivity : AppCompatActivity() {

    private var recyclerView:RecyclerView ? = null
    private var adapter: RecyclerView.Adapter<*>? = null
    private var layoutManager:RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles_miguel)

        recibeServicio()


    }


   override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflat= menuInflater
        inflat.inflate(R.menu.menu_back_miguel, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {


        when(item!!.itemId){
            R.id.back_miguel -> {

                val intent = Intent(this@DetallesMiguelActivity, AdaptadorFragments::class.java)
                startActivity(intent)
                return true
            }else -> return super.onOptionsItemSelected(item)
        }
    }



    fun recibeServicio(){

        val pref: SharedPreferences = getSharedPreferences(InicioActivity.NAME, MODE_PRIVATE)
        val editor: SharedPreferences.Editor = pref.edit()

        val recibe = GetApi.crearCliente()
        val call = recibe.getResponseGet()

        call.enqueue(object : retrofit2.Callback<List<ModelGet>>{



            override fun onResponse(call: Call<List<ModelGet>>, response: Response<List<ModelGet>>) {

                val respuesta: List<ModelGet>

                val detalles =  response.body()!!

                respuesta = detalles


                var lista:ArrayList<Int> = ArrayList()

                lista.add(1)
                lista.add(2)
                lista.add(3)

                Collections.sort(lista)

                for (numero in lista) {
                    println(numero)
                }







                editor.putString("genero", respuesta[0].sexo)
                editor.putString("localidad",  respuesta[0].localidad)
                editor.putString("photo",  respuesta[0].photo)
                editor.apply()

                recyclerView = findViewById(R.id.recycler_detalles_get)
                layoutManager = LinearLayoutManager(this@DetallesMiguelActivity)

                adapter = RecyclerAdapter(
                        respuesta,
                        R.layout.detalles_adapter_miguel
                        )

                recyclerView!!.layoutManager = layoutManager
                recyclerView!!.adapter = adapter

            }

            override fun onFailure(call: Call<List<ModelGet>>, t: Throwable) {
                Toast.makeText(applicationContext,"Fallo la coneccion",Toast.LENGTH_SHORT).show()
            }




        })

    }
}
