package com.bbva.intranet.baz.miguel.apismiguel

import com.bbva.intranet.baz.miguel.modelmiguel.ResponsePost
import com.bbva.intranet.baz.miguel.modelmiguel.SendPost
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface PostApi {


    @POST("/personal")
    fun sendPost(@Body sendPost: SendPost): Call<ResponsePost>

    companion object{
        fun crearCliente(): PostApi {


            val retrofit = Retrofit.Builder()
                    .baseUrl("http://5ca637e23a082600142794d8.mockapi.io/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(PostApi::class.java)


        }




    }



}