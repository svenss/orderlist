package com.bbva.intranet.baz

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.bbva.intranet.baz.miguel.viewmiguel.InicioActivity
import com.bbva.intranet.baz.miguel.viewmiguel.SplashActivityMiguel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var irPantalla = findViewById<TextView>(R.id.tv_mipantalla)

        irPantalla.setOnClickListener {

            val intent = Intent(this, SplashActivityMiguel::class.java)
            startActivity(intent)
        }
    }
}
